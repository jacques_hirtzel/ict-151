<?php
Class Personne EXTENDS Projet{
   
   private $id_per;
   private $nom;
   private $prenom; 
   private $email;
   private $password;
   private $news_letter;
  
  
    public function __construct($id = null){
        
        parent::__construct();
        
        if($id){
            $this->set_id_per($id);
            $this->init();
        }
      
    }
   
   /**
     * Initialisation de l'objet (l'id doit être setté)
     * @return boolean
     */
    public function init() {
       $query = "SELECT * FROM t_personnes WHERE id_per=:id_per";
       try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_per'] = $this->get_id_per();
            
            $stmt->execute($args);
            $tab = $stmt->fetch();
            
            $this->set_nom($tab['nom_per']);
            $this->set_prenom($tab['prenom_per']);
            $this->set_email($tab['email_per']);
            $this->set_password($tab['password_per']);
            $this->set_news_letter($tab['news_letter_per']);
           
        } catch (Exception $e) {
            return false;
        }
         return true;
    }
    
     public function __toString(){
        $str = "\n<pre>\n";
        foreach($this AS $key => $val){
            if($key != "pdo"){
                $str .= "\t".$key;
                $lengh_key = strlen($key);
                for($i=strlen($key);$i<20;$i++){
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;".$val."\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }
   
    
    
    /**
     * Initialisation de l'objet personne via son e-mail (unique)
     * @return boolean
     */
    public function init_by_mail($email) {
       $query = "SELECT * FROM t_personnes WHERE email_per=:email_per";
       try {
            $stmt = $this->pdo->prepare($query);
            $args[':email_per'] = $email;
            $stmt->execute($args);
            $tab = $stmt->fetch();
            
            $this->set_id_per($tab['id_per']);

        } catch (Exception $e) {
            return false;
        }
         return true;
    }
    
   
   
   /**
    * Vérifie si une personne est active dans une  partie
    * @param int $id_prt ID de la partie
    * @return int nombre de partie dans lesqelles la personne est active (status_jou = 1)
    */
   public function is_joueur($id_prt){
      $query = "SELECT COUNT(id_jou) AS is_joueur FROM t_joueurs JOU WHERE id_per=:id_per AND id_prt = :id_prt AND status_jou=1 LIMIT 1";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_per'] = $this->get_id_per();
            $args[':id_prt'] = $id_prt;
            $stmt->execute($args);
            $tab = $stmt->fetch();
        } catch (Exception $e) {
            return false;
        }
        return $tab['is_joueur'];
      
   }

   /**
    * Ajoute une personne dans la base de données
    * @param array tableau avec les propriétés de la personne
    * @return int id de la personne ajoutée
    */
   public function add($tab){
       
       $this->gen_password($tab['password']);
      
      // Tableau d'arguments
        $args['nom_per'] = $tab['nom_per'];
        $args['prenom_per'] = $tab['prenom_per'];
        $args['email_per'] = $tab['email_per'];
        $args['password_per'] = $this->get_password();
        $args['news_letter_per'] = $tab['news_letter_per'];
        
        $query = "INSERT INTO t_personnes SET "
                . "nom_per = :nom_per, "
                . "prenom_per = :prenom_per, "
                . "password_per = :password_per, "
                . "email_per = :email_per, "
                . "news_letter_per = :news_letter_per";
         try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            
        } catch (Exception $e) {
           //echo $e;
            return false;
        }
        return $this->pdo->lastInsertId();
       
    }
    
    /**
     * Récupère la totalité de enregistrements de la table personne dans l'ordre fourni 
     * @param string $order ordre à utiliser (par défaut :  nom, prénom
     * @return array tableau des personnes
     */
    public function get_all($order = "nom_per, prenom_per"){
        
        $args[":order"] = $order;
        
        $query = "SELECT * FROM t_personnes ORDER BY :order";
        
        try{
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return($tab);
        }catch (Exception $e){
            return false;
        }
    }
   
   
    public function get_all_aut(){
       
        $query = "SELECT code_aut FROM t_aut_fnc AUF "
                ."JOIN t_autorisations AUT ON AUF.id_aut=AUT.id_aut "
                ."JOIN t_fonctions FNC ON AUF.id_fnc=FNC.id_fnc "
                ."JOIN t_fnc_per FNP ON FNC.id_fnc=FNP.id_fnc AND FNP.id_per=:id_per";
                
        $args['id_per'] = $this->get_id_per();
        //print_r($args);
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab = $stmt->fetchAll();
        
        foreach($tab AS $aut){
            $tab_aut[] = $aut['code_aut'];
        }
        
        
        return($tab_aut);
   }
   
   
     public function get_all_fnc(){
       
        $query = "SELECT abr_fnc,nom_fnc FROM t_fonctions FNC "
                ."JOIN t_fnc_per FNP ON FNC.id_fnc=FNP.id_fnc AND FNP.id_per=:id_per ";
        try {
            $args['id_per'] = $this->get_id_per();
            $stmt = $this->pdo->prepare($query);
            
            if($stmt->execute($args)){
                $tab_aut = $stmt->fetchAll();
                return $tab_aut;
            }else{
                return false;
            }
        } catch (Exception $e) {
            return false;
        }    
   }
   
   
   public function check_aut($aut_list){
       
       $tab_aut = explode(";",$aut_list);
       //print_r($tab_aut);
       
       $tab_aut_per = $this->get_all_aut();
       
       foreach($tab_aut AS $aut){
           if(in_array($aut,$tab_aut_per)){
               return true;
           }
        }
        return false;
   }
   
   
    /**
     * ajoute une fonction à la personne
     * @param int $id_fnc id de la fonction
     * @return boolean Vrai =  fonction ajoutée Faux = fonction non ajoutée
     */
    public function add_fnc($id_fnc){
       $query = "INSERT INTO t_fnc_per SET id_per=:id_per, id_fnc=:id_fnc";   
        
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_per'] = $this->get_id_per();
            $args[':id_fnc'] = $id_fnc;
            if($stmt->execute($args)){
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }


    /**
     * Rerire une fonction à la personne
     * @param int $id_fnc id de la fonction
     * @return boolean Vrai =  fonction retirée Faux = fonction non retirée
     */
    public function del_fnc($id_fnc){
        $query = "DELETE FROM t_fnc_per WHERE id_per=:id_per AND id_fnc=:id_fnc";   
        
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':id_per'] = $this->get_id_per();
            $args[':id_fnc'] = $id_fnc;
            $stmt->execute($args);
           if($stmt->execute($args)){
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
        
    }


   
   
   /**
     * Vérifie si un email existe dans la base de données
     * @param string $email
     * @return boolean Vrai =  email exist Faux = email non trouvé
     */
    public function check_email($email){
        $query = "SELECT * FROM t_personnes WHERE email_per = :email LIMIT 1";
        try {
            $stmt = $this->pdo->prepare($query);
            $args[':email'] = $email;
            $stmt->execute($args);
            $tab = $stmt->fetch();
            if($tab['email_per'] == $email){
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
    
  /**
    * Vérifie si la combinaison email-password existe bien dans la base de données
    * @param string $email
    * @param string $password (de l'utilisateur non crypté)
    * @return id de la persone = combinaison existe / Faux = combinaison n'existe pas
    */
   public function check_login($email,$password){
       $query = "SELECT id_per,password_per FROM t_personnes WHERE email_per=:email LIMIT 1";
       try {
         $stmt = $this->pdo->prepare($query);
         $args[':email'] = $email;
         $stmt->execute($args);
         $tab = $stmt->fetch();
         if(password_verify($password,$tab['password_per'])){
            $_SESSION['id'] = $tab['id_per'];
            $user_browser_ip = $_SERVER['HTTP_USER_AGENT'].$_SERVER['REMOTE_ADDR'];
            $_SESSION['login_string'] = password_hash($tab['password_per'].$user_browser_ip, PASSWORD_DEFAULT);
            $_SESSION['email'] = $email;
            return true;
         }else{
            return false;
         }
       }  catch (Exception $e) {
         return false;
      }
   }
    
    /**
     * Vérifie si la login string en session est identique à celle calculée à l'instant
     * @return boolean true = ok false = ko
     */
    public function check_connect(){
       if (isset($_SESSION['id'],$_SESSION['email'],$_SESSION['login_string'])) {
          $user_browser_ip = $_SERVER['HTTP_USER_AGENT'].$_SERVER['REMOTE_ADDR'];
          if(password_verify($this->get_password().$user_browser_ip,$_SESSION['login_string'])){
                return true;
            }else{
                return false;
            }
       }else{
            return false;
       }
    }
    
   
   /**
     * Crypte le mot de passe à partit de l'argument $password et in initialise la propriété password
     * @param string $password mot de passe en clair
     */
    public function gen_password($password) {
        $this->set_password(password_hash($password, PASSWORD_DEFAULT));
    }
    
    
     /**
   * Set la propriété nom de la class
   * @param string $nom 
   */
   public function set_id_per($id_per) {
     $this->id_per = $id_per;
   }
   
   /**
   * Get la propriété nom de la class
   * @return string $nom 
   */
   public function get_id_per() {
     return $this->id_per;
   }
    
   
   /**
   * Set la propriété nom de la class
   * @param string $nom 
   */
   public function set_nom($nom) {
     $this->nom = $nom;
   }
   
   /**
   * Get la propriété nom de la class
   * @return string $nom 
   */
   public function get_nom() {
     return $this->nom;
   }
   
   /**
   * Set la propriété prénom de la class
   * @param string $prenom 
   */
   public function set_prenom($prenom) {
     $this->prenom = $prenom;
   }
   
   /**
   * Get la propriété prénom de la class
   * @return string $prenom 
   */
   public function get_prenom() {
     return $this->prenom;
   }
   
   /**
   * Set la propriété email de la class
   * @param string $email 
   */
   public function set_email($email) {
     $this->email = $email;
   }
   
   /**
   * Get la propriété email de la class
   * @return string $email 
   */
   public function get_email() {
     return $this->email;
   }
   
   /**
   * Set la propriété password de la class
   * @param string $password (crypté) 
   */
   public function set_password($password) {
     $this->password = $password;
   }
   
   /**
   * Get la propriété password de la class
   * @return string $password 
   */
   public function get_password() {
     return $this->password;
   }
   
   /**
   * Set la propriété new_letter de la class (0 oi 1)
   * @param int $news_letter 
   */
   public function set_news_letter($news_letter) {
     if (is_int($news_letter*1)) {
         $this->news_letter = $news_letter;
     } else {
         $this->news_letter = 1;
     }
   }
   
   /**
   * Get la propriété news_letter de la class
   * @return string $news_letter 
   */
   public function get_news_letter() {
     return $this->news_letter;
   }
}
?>