<?php 
header('Content-Type: application/json');
session_start();
require("./../../config/config.inc.php");
require_once(WAY."/includes/autoload.inc.php");

   $fnc = new Fonction();
   
   $id = $fnc->add($_POST);
   
   $fnc->set_id_fnc($id);
   if($fnc->init()){
      $tab['reponse'] = true;
      $tab['message']['texte'] = "La fonction ".$fnc->get_nom()." (".$fnc->get_abreviation().") à bien été ajoutée";
      $tab['message']['type'] = "success";
   }else{
      $tab['reponse'] = false;
      $tab['message']['texte'] = "Un problème est survenu";
      $tab['message']['type'] = "danger";
   }
//print_r($_POST);
echo json_encode($tab);
?>