$(function(){
    $(".auth").click(function(){
        if($(this).is(":checked")){
            console.log("Activation de la fonction "+$(this).attr("id_fnc")+ " pour la personne "+$(this).attr("id_per"));
        }else{
            console.log("Desctivation de la fonction "+$(this).attr("id_fnc")+ " pour la personne "+$(this).attr("id_per"));
        }

        /* Remplace la checkbox pendant le traitemenr */
        $(this).parent().append("<img class=\"loading\" src=\"./../icones/loading.gif\" height=38px>");
        $(this).css("display","none");
        
        /* bloque tout l'écran pendant le traitement */ 
        //$("#loading").css("display","block");
        
        $.post(
            "./json/add_del_fnc_per.json.php?_="+Date.now(),
            {
                id_per:$(this).attr("id_per"),
                id_fnc:$(this).attr("id_fnc"),
                status:$(this).is(":checked"),
                id_auth:$(this).attr("id")
            },
            function(data,status){
                if(data.operation == "add"){
                    $("#"+data.id_auth).parent().css("background","lightgreen");
                }else{
                    $("#"+data.id_auth).parent().css("background","#F99494");
                }
                message(data.message.texte,data.message.type);
                
                /* Remplace la checkbox pendant le traitemenr */
                $("#"+data.id_auth).siblings('.loading').remove();
                $("#"+data.id_auth).css("display","block");
                
                /* bloque tout l'écran pendant le traitement */ 
                //$("loading").css("display","none");        
            }
        );
    });
});