<?php
session_start();
require("./../config/config.inc.php");
$aut = "ADM_FNC";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
?>
<div class="row">
      <div class="header">
         <h3></h3>
      </div>
   </div>
    
    
   <div class="col-md-12">
      <div class="panel panel-primary ">
         <div class="panel-heading">
            Attribution des fonctions
         </div>
         <div class="panel-body">
            <?php
            $fnc = new Fonction();
               
               $tab_fnc = $fnc->get_all();
               
               $tab_per_fnc = $fnc->get_tab_per_all_fnc();
               /*echo "<pre>";
               print_r($tab_per_fnc);
               echo "</pre>";*/
            ?>
            
            
            <table class="table table-bordered fnc_per loading_td">
               <tr>
                  <th>Nom, prénom</th>
                  <?php
                  foreach($tab_fnc as $fonction){
                     echo "<th>";
                     echo $fonction['nom_fnc'];
                     echo "</th>";
                  }
                  ?>
               </tr>
               <?php
               $tab_per = $per->get_all();
               
               
               
              // echo "<pre>";
               //int_r($tab_per);
               //echo "</pre>";
               foreach ($tab_per as $personne) {
                  echo "<tr>";
                  echo "<td>";
                  echo $personne['nom_per']." ".$personne['prenom_per'];
                  echo "</td>";
                  
                  foreach ($tab_fnc as $fonction){
                     echo "<td>";
                     echo "<input type=\"checkbox\" id=\"auth_".$personne['id_per']."_".$fonction['id_fnc']."\" class=\"form-control auth\" id_fnc=\"".$fonction['id_fnc']."\" id_per=\"".$personne['id_per']."\" ";
                     if(isset($tab_per_fnc[$fonction['id_fnc']])){
                        if(in_array($personne['id_per'],$tab_per_fnc[$fonction['id_fnc']])){
                           echo " checked=\"checked\" ";
                        }
                     }
                     echo " >";
                     echo "</td>";
                  }
                     
                  
                  echo "</tr>";
               }
               
               ?>
            </table>
         </div>
                
      </div>    
   </div>

</div>
<script src="./js/attribution_fnc_per.js"></script>