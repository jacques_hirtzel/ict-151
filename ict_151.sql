-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 21 déc. 2018 à 09:40
-- Version du serveur :  10.1.37-MariaDB
-- Version de PHP :  7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ict_151`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_autorisations`
--

CREATE TABLE `t_autorisations` (
  `id_aut` int(11) UNSIGNED NOT NULL,
  `nom_aut` varchar(100) NOT NULL,
  `code_aut` varchar(10) NOT NULL,
  `desc_aut` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_autorisations`
--

INSERT INTO `t_autorisations` (`id_aut`, `nom_aut`, `code_aut`, `desc_aut`) VALUES
(1, 'Gestion des utilisateurs', 'ADM_USR', 'Gestion complète des utilisateurs, permet entres autres choses la suppression des utilisateurs'),
(2, 'Gestion des utilisateurs', 'USR_USR', 'Gestion complète des utilisateurs'),
(3, 'Utilisation du site', 'ADM_SIT', 'Cette autorisation n\'est pas utilisée'),
(4, 'Utilisation du site', 'USR_SIT', 'Accès aux parties publiques du site'),
(5, 'Droits d\'accès', 'ADM_ARU', 'Création des droits d\'accès et attribution des droits d\'accès des droits d\'accès aux fonctions'),
(6, 'Droits d\'accès', 'USR_ARU', 'Attribution des droits d\'accès des droits d\'accès aux fonctions'),
(7, 'Fonction utilisateurs', 'ADM_FNC', 'Création et attribution des fonctions aux utilisateurs'),
(8, 'Fonction utilisateurs', 'USR_FNC', 'Attribution des fonctions aux utilisateurs'),
(9, 'Gestion des autorisations', 'ADM_AUT', 'Création d\'autorisations'),
(10, 'Gestion des autorisations', 'USR_AUT', 'Cette autorisation n\'est pas utiliseée');

-- --------------------------------------------------------

--
-- Structure de la table `t_aut_fnc`
--

CREATE TABLE `t_aut_fnc` (
  `id_aut` int(10) UNSIGNED NOT NULL,
  `id_fnc` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_aut_fnc`
--

INSERT INTO `t_aut_fnc` (`id_aut`, `id_fnc`) VALUES
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2);

-- --------------------------------------------------------

--
-- Structure de la table `t_fnc_per`
--

CREATE TABLE `t_fnc_per` (
  `id_fnc` int(10) UNSIGNED NOT NULL,
  `id_per` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_fnc_per`
--

INSERT INTO `t_fnc_per` (`id_fnc`, `id_per`) VALUES
(2, 1),
(3, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Structure de la table `t_fonctions`
--

CREATE TABLE `t_fonctions` (
  `id_fnc` int(10) UNSIGNED NOT NULL,
  `nom_fnc` varchar(100) NOT NULL,
  `abr_fnc` varchar(10) NOT NULL,
  `desc_fnc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_fonctions`
--

INSERT INTO `t_fonctions` (`id_fnc`, `nom_fnc`, `abr_fnc`, `desc_fnc`) VALUES
(2, 'Administrateur', 'admin', 'Administrateur de la plateforme, bénéficie de tous les accès'),
(3, 'Gestionnaire des droits d\'accès', 'Gest Accès', 'Utilisateur pouvant gérer les droits d\'accès des autres utilisateurs');

-- --------------------------------------------------------

--
-- Structure de la table `t_personnes`
--

CREATE TABLE `t_personnes` (
  `id_per` int(10) UNSIGNED NOT NULL,
  `nom_per` varchar(30) NOT NULL,
  `prenom_per` varchar(30) NOT NULL,
  `email_per` tinytext NOT NULL,
  `password_per` varchar(255) NOT NULL,
  `sel_per` varchar(10) NOT NULL,
  `news_letter_per` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_personnes`
--

INSERT INTO `t_personnes` (`id_per`, `nom_per`, `prenom_per`, `email_per`, `password_per`, `sel_per`, `news_letter_per`) VALUES
(1, 'bob', 'sponge', 'bob@water.us', '$2y$10$WFn.dVZAH9oSZP76jbtXlu3FbqxsM7IufBz4pXEC3pvpYY76G8Yn.', '', 1),
(2, 'tom', 'sponge', 'tom@water.us', '$2y$10$/LYs4UIttKKH0yiDKrv/I.7XDx3Vxxx1j8ybUAinZ7iPqoFDge0iG', '', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `t_autorisations`
--
ALTER TABLE `t_autorisations`
  ADD PRIMARY KEY (`id_aut`);

--
-- Index pour la table `t_aut_fnc`
--
ALTER TABLE `t_aut_fnc`
  ADD PRIMARY KEY (`id_aut`,`id_fnc`),
  ADD KEY `id_fnc` (`id_fnc`);

--
-- Index pour la table `t_fnc_per`
--
ALTER TABLE `t_fnc_per`
  ADD PRIMARY KEY (`id_fnc`,`id_per`),
  ADD KEY `id_per` (`id_per`);

--
-- Index pour la table `t_fonctions`
--
ALTER TABLE `t_fonctions`
  ADD PRIMARY KEY (`id_fnc`);

--
-- Index pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  ADD PRIMARY KEY (`id_per`),
  ADD KEY `id_per` (`id_per`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `t_autorisations`
--
ALTER TABLE `t_autorisations`
  MODIFY `id_aut` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `t_fonctions`
--
ALTER TABLE `t_fonctions`
  MODIFY `id_fnc` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  MODIFY `id_per` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `t_aut_fnc`
--
ALTER TABLE `t_aut_fnc`
  ADD CONSTRAINT `t_aut_fnc_ibfk_1` FOREIGN KEY (`id_aut`) REFERENCES `t_autorisations` (`id_aut`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_aut_fnc_ibfk_2` FOREIGN KEY (`id_fnc`) REFERENCES `t_fonctions` (`id_fnc`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `t_fnc_per`
--
ALTER TABLE `t_fnc_per`
  ADD CONSTRAINT `t_fnc_per_ibfk_1` FOREIGN KEY (`id_per`) REFERENCES `t_personnes` (`id_per`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_fnc_per_ibfk_2` FOREIGN KEY (`id_fnc`) REFERENCES `t_fonctions` (`id_fnc`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
