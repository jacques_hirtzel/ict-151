<?php 
require_once(WAY."/includes/autoload.inc.php");

//print_r($_SESSION);

   if(isset($_SESSION['id'])){
      $per =  new Personne($_SESSION['id']);
   
      if(!$per->check_connect()){
         session_destroy();
         header('Location: '.URL.'/login.php');
         exit;
      }
   }else{
         session_destroy();
         header('Location: '.URL.'/login.php');
         exit;
   }


if(!$per->check_aut($aut)){
  // session_destroy();
   header('Location: '.URL.'/index.php');
   exit;
}

?>